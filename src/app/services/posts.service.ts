import {Injectable} from '@angular/core';
import {Post} from '../models/post';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {User} from '../models/user';

@Injectable()
export class PostsService {

  static apiHost = 'http://localhost:3000';

  constructor(private http: HttpClient) {
  }

  static getHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }

  // GET /v1/posts
  getPosts(): Observable<Post[]> {
    const headers = PostsService.getHeaders();

    return this.http.get(
      PostsService.apiHost + '/posts',
      {headers: headers},
    ).pipe(
      map((response) => {
        return JSON.parse(JSON.stringify(response), Post.reviver);
      }),
    );
  }

  // GET /v1/posts/1
  getPost(id: number): Observable<Post> {
    const headers = PostsService.getHeaders();

    return this.http.get(
      PostsService.apiHost + '/posts/' + id,
      {headers: headers},
    ).pipe(
      map((response) => {
        return JSON.parse(JSON.stringify(response), Post.reviver);
      }),
    );
  }

  // POST /v1/posts/create
  createPost(post: Post): Observable<any> {
    const headers = PostsService.getHeaders();

    return this.http.post(
      PostsService.apiHost + '/posts',
      JSON.stringify(post),
      {headers: headers},
    ).pipe(
      map((response) => {
        return JSON.parse(JSON.stringify(response), Post.reviver);
      }),
    );
  }

  // PUT /v1/posts/1
  updatePost(post: Post): Observable<any> {
    const headers = PostsService.getHeaders();

    return this.http.put(
      PostsService.apiHost + '/posts/' + post.id,
      JSON.stringify(post),
      {headers: headers},
    ).pipe(
      map((response) => {
        return JSON.parse(JSON.stringify(response), Post.reviver);
      }),
    );
  }

  // DELETE /v1/posts/1
  deletePost(id: number): Observable<any> {
    const headers = PostsService.getHeaders();

    return this.http.delete(
      PostsService.apiHost + '/posts/' + id,
      {headers: headers},
    ).pipe(
      map((response) => {
        return JSON.parse(JSON.stringify(response), Post.reviver);
      }),
    );
  }

  // GET /v1/posts
  getUsers(): Observable<User[]> {
    const headers = PostsService.getHeaders();

    return this.http.get(
      PostsService.apiHost + '/users',
      {headers: headers},
    ).pipe(
      map((response) => {
        return <User[]>response;
      }),
    );
  }
}
