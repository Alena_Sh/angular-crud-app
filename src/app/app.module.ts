import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {PostsService} from './services/posts.service';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {EditPostComponent} from './components/edit-post/edit-post.component';
import {PostsListComponent} from './components/posts-list/posts-list.component';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const appRoutes: Routes = [
  {
    path:       '',
    redirectTo: '/posts',
    pathMatch:  'full',
  },
  {
    path:     'posts',
    children: [
      {
        path:      '',
        component: PostsListComponent,
        data:      {title: 'Posts List'},
        pathMatch:  'full',
      },
      {
        path:      'create',
        component: EditPostComponent,
        data:      {title: 'Create post'},
        pathMatch:  'full',
      },
      {
        path:      ':id',
        component: EditPostComponent,
        data:      {title: 'Edit post'},
      },
    ],
  },
  {path: '**', component: PageNotFoundComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    EditPostComponent,
    PostsListComponent,
    PageNotFoundComponent,
  ],
  imports:      [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers:    [
    PostsService,
  ],
  bootstrap:    [AppComponent],
})
export class AppModule {
}
