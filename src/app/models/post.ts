export class Post {
  static dateAttributes = ['created_at', 'updated_at'];

  id: number;
  user_id: number;
  title: string;
  preview: string;
  content: string;
  author: string;
  created_at: number;
  updated_at: number;
  isCollapsed?: boolean;

  static reviver(key: string, value: any): any {
    if (Post.dateAttributes.includes(key) && value !== null) {
      value = value * 1000;
    }

    return value;
  }
}



