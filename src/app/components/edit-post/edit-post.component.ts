import {Component} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {PostsService} from '../../services/posts.service';
import {AppComponent} from '../../app.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Post} from '../../models/post';
import {User} from '../../models/user';

@Component({
  selector:    'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls:   ['./edit-post.component.scss'],
})
export class EditPostComponent {

  static MODE_CREATE = 10;
  static MODE_UPDATE = 20;

  post: Post;
  users: User[];
  form: FormGroup;
  submitted = false;
  mode: number;

  constructor(private service: PostsService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private fb: FormBuilder,
              private app: AppComponent) {

    this.activatedRoute.params
        .subscribe((params: Params) => {
          if (typeof params['id'] !== 'undefined') {
            this.mode = EditPostComponent.MODE_UPDATE;
            this.getPost(params['id']);
          } else {
            this.mode = EditPostComponent.MODE_CREATE;
            this.newPost();
          }
        });

    this.form = this.fb.group({
      user_id: ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
      title:   ['', Validators.compose([Validators.required, Validators.minLength(5)])],
      preview: [''],
      content: [''],
    });
  }

  static truncate(value: string, limit: number = 20, trail: String = '…'): string {
    let result = value || '';

    if (value) {
      const words = value.split(/\s+/);
      if (words.length > Math.abs(limit)) {
        if (limit < 0) {
          limit *= -1;
          result = trail + words.slice(words.length - limit, words.length).join(' ');
        } else {
          result = words.slice(0, limit).join(' ') + trail;
        }
      }
    }

    return result;
  }

  getPost(id: number) {
    this.service.getPost(id)
        .subscribe(
          post => {
            this.post      = post;
            this.app.title = 'Edit post: ' + this.post.title;
            this.getUsers();
          },
          error => {
            this.handleError(error);
          },
        );
  }

  getUsers() {
    this.service.getUsers()
        .subscribe(
          users => {
            this.users = users;
          },
          error => this.handleError(error),
        );
  }

  newPost() {
    this.post      = new Post();
    this.app.title = 'New post';
    this.getUsers();
  }

  onSubmit(): void {
    this.submitted = true;
    const author   = this.users.find(user => {
      return user.id === Number(this.post.user_id);
    });

    if (author) {
      this.post.author = author.name;
    }
    if (!this.post.preview) {
      this.post.preview = EditPostComponent.truncate(this.post.content);
    }

    if (this.mode === EditPostComponent.MODE_UPDATE) {
      this.post.updated_at = Math.round((Date.now() / 1000));

      this.service.updatePost(this.post)
          .subscribe(
            result => {
              this.handleSuccess(result);
            },
            error => {
              this.handleError(error);
            },
          );
    } else {

      this.post.created_at = Math.round((Date.now() / 1000));
      this.post.updated_at = Math.round((Date.now() / 1000));

      this.service.createPost(this.post)
          .subscribe(
            result => {
              this.handleSuccess(result);
            },
            error => {
              this.handleError(error);
            },
          );
    }
  }

  isValid(field): boolean {
    let isValid = false;
    // If the field is not touched and invalid, it is considered as initial loaded form. Thus set as true
    if (this.form.controls[field].touched === false) {
      isValid = true;
    } else if (this.form.controls[field].touched === true && this.form.controls[field].valid === true) {
      isValid = true;
    }
    return isValid;
  }

  handleSuccess(result: any) {
    this.submitted = false;
    this.router.navigate(['/']);
  }

  handleError(error: any) {
    this.submitted = false;
    console.error(error);
  }
}
