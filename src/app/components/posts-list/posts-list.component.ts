import {Component, OnInit} from '@angular/core';
import {Post} from '../../models/post';
import {PostsService} from '../../services/posts.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppComponent} from '../../app.component';

@Component({
  selector:    'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls:   ['./posts-list.component.scss'],
})
export class PostsListComponent implements OnInit {
  posts: Post[];

  constructor(private service: PostsService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private app: AppComponent) {
    this.showPosts();

    this.activatedRoute.data.subscribe(data => {
      this.app.title = data.title;
    });
  }

  ngOnInit() {
  }

  editPost(id: number) {
    this.router.navigate(['posts/' + id]).then();
  }

  deletePost(id: number) {
    this.service.deletePost(id).subscribe(response => {
        this.showPosts();
      },
      error => {
        console.error('Error occurred:', error);
      });
    return false;
  }

  private showPosts() {
    this.service.getPosts().subscribe(
      response => {
        this.posts = response;
      },
      error => {
        console.error('Error occurred:', error);
      },
    );
  }
}
